<?php include_once('functions/functions.php');

$id = $_GET['id'];
$table = $_GET['table'];

session_start();
editInfoRecord($id, $table);
logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Pandora Network - BCS.net.nz</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <?php if( $_SESSION['login'] == TRUE )
    {     
        ?><body onload="loadNav('navAdmin')"><?php                      
    }
    else
    {
        ?><body onload="loadNav('nav')"><?php
    }?>
    <body>
        <!--Header Image-->
        <div class="header-image">
            <img src="images/logo1.png" class="img-responsive">
        </div>

        <div id="navDiv">
        </div>
        <div class="container">

        <?php 
            if( $_SESSION['login'] == TRUE )
            {     
                ?>
                <div class="jumbotron">
                <h1>Edit a Panel</h1>
                <form method="POST">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Add Section Heading</span>
                        <input type="text" class="form-control" name="section" placeholder="Section Heading" aria-describedby="basic-addon1">
                    </div>
                <br>
                <div class="panel panel-primary">
                    <div class="panel-heading infopanel">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Panel Heading</span>
                                <input type="text" class="form-control" name="heading" placeholder="Heading" aria-describedby="basic-addon1">
                            </div>

                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="content:">Panel Content:</label>
                            <textarea class="form-control" rows="5" name="content"></textarea>
                        </div>
                        <br>
                    </div>
                </div>
                <button type="submit" name="updateInfoItem" class="btn btn-success">Edit Panel</button>
                </form>
                </div><?php
            }
            else
            {
                ?><div class="panel panel-primary">
                    <div class="panel-heading infopanel">
                        <h3 class="panel-title">Error: You do not have access to this page</h3>
                    </div>
                    <div class="panel-body">
                    <div class="panel-body">
                        <p>Please try again after logging in.</p>
                    </div>
                  </div><?php
            }
        ?>
    
        <!--footer-->
        <footer class="footer">
            <div class="container">
                <?php 
                    if( $_SESSION['login'] == TRUE )
                    {     
                        ?><form method='POST' >
                            <input class="btn btn-default" type="submit" name="logout" value="logout">
                        </form>
                        <span class="text-muted">Demo BCS.net Design - Group 2 - Ashton Lye, Cody Ludwig, Liam Mason-Webb</span><?php
                    }
                    else
                    {
                        ?><span class="text-muted">Demo BCS.net Design - Group 2 - Ashton Lye, Cody Ludwig, Liam Mason-Webb</span><?php
                    }
                ?>
            </div>
        </footer>

    <script src= "js/script.js"></script>
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>