CREATE DATABASE IF NOT EXISTS `comp6002_17a_assn3` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `comp6002_17a_assn3`;


DROP TABLE IF EXISTS `Home_Page`;
CREATE TABLE `Home_Page` (
  `Content_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Heading` varchar(200) NOT NULL,
  `Content` varchar(2000) NOT NULL,
  `Button` varchar(200) NOT NULL,
  `Link` varchar(200) NOT NULL,
  PRIMARY KEY (`Content_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `Home_Page` (`Content_ID`, `Heading`, `Content`, `Button`,`Link`) VALUES
('1', 'Welcome to Pandora', 'Toi Ohomai Institute of Technology & University of Waikato Bachelor of Computing and Mathematical Science. <br>This is a seperate intranet for students who are completing the Bachelor of Computer Science in Applied Science. This website allows you to access information about the Pandora network provides quick access to various sites and resources you will use in your studies and also features guides on how to set up your Google Apps Slack and Microsoft Imagine accounts.', 'More Info', 'info.php');


DROP TABLE IF EXISTS `INFO_Page`;
CREATE TABLE `INFO_Page` (
  `Content_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Section` varchar(200) ,
  `Heading` varchar(200) NOT NULL,
  `Content` varchar(2000) NOT NULL,
  PRIMARY KEY (`Content_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `INFO_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(1,'Pandora Info', 'Your Files', 'The corporate network (the library and classrooms other than the 3rd floor) is completely separate from the Pandora network (DT219, DT303, DT308 and DT312). Both networks have an network drive for you to store you files in and both networks refer to that drive as H-Drive, however they are different and do not relate to each other in any way. By using your Google Apps account you are able to access your files from any machine using a web browser and you will not be limited by this.');
INSERT INTO `INFO_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(2, '','Toi Ohomai Wi-Fi Network', 'The student Wi-Fi is also completely separate from the Pandora Network. If you are using your own device or connect your mobile device, the easiest way to access your files is by using Google Apps. The Wifi network basically allows you to browse the web. There are some exceptions, but you will hear about that in the Web Development papers you will do in your degree.');
INSERT INTO `INFO_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(3, '','Can I access the anything on the Pandora Network from Home?', 'Simple answer - No you cannot. However your course material is on Moodle and you can put files on Google drive and you are able to access what you need from there. In terms of software, everything we use in the labs is available for free either through MS Imagine or a student service or the program is actually free to download.');

DROP TABLE IF EXISTS `SETUP_Page`;
CREATE TABLE `SETUP_Page` (
  `Content_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Section` varchar(200) ,
  `Heading` varchar(200) NOT NULL,
  `Content` varchar(2000) NOT NULL,
  PRIMARY KEY (`Content_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(1,'Setting Up Google Apps', 'Set Up Google Apps', 'With chrome you can sign into your google account from the browser app and on the webpage. If you do it from the browser app, then it will create a link to your google profile and sign into your personal account and the school account at the same time.<br>Have a look at this gif to see how to sign into Chrome.<br><br><img src="images/googlesetup.gif" class="img-rounded">');
INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(2,'', 'Set Up Your Google Account', 'At the beginning of each semester, all the passwords are reset to 12345678.<br>When you sign in with Chrome you will need to know your student Id.<br><br>Your Google Apps ID = your student number@bcs.net.nz - for example 1234567@bcs.net.nz<br>Your password is 12345678 - You will be asked to change this after you accept the terms and conditions.');

INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(3,'Setting Up Microsoft Imagine','What is Microsoft Imagine?', 'At the beginning of each semester we set the students up with a Microsoft Imagine Account.<br>This allows you to download some free products from Microsoft that you would otherwise have to pay for.<br>Once you have graduated or are no longer with us, you can keep the software and licenses, but you will not be able to access the website anymore.');
INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(4,'', 'Set Up Your Microsoft Imagine Account', 'Log into your bcs.net.nz email - it\'s the same as the google apps account which you have (hopefully) already set up - you should have an email from Toi Ohomai and the subject should say "Your school created a Microsoft Imagine WebStore account for you."<br><br>In the video below you can see how to setup your account.<br><br><img src="images/imaginesetup1.gif" class="img-rounded">');
INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(5, '', 'Downloading Software Through Microsoft Imagine', 'In the video below you can see how to downoad and install software through Microsoft Imagine.<br><br><img src="images/imaginesetup2.gif" class="img-rounded">');

INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(6, 'Set Up Your Slack Account', 'Set Up Your Slack Account', 'Within the IT Team and some of your courses we use slack to communicate.<br>To create your account simply go to <a href="https://to-bcs.slack.com">https://to-bcs.slack.com</a> and sign in with your Google Apps Account.<br><br><img src="images/slacksetup.gif" class="img-rounded">');
INSERT INTO `SETUP_Page` (`Content_ID`,`Section`, `Heading`, `Content`) VALUES
(7, '', 'Slack on Your Mobile Device', 'Slack has a (mobile) app for every platform basically.<br><br>Simply go to <a href="https://slack.com/downloads">https://slack.com/downloads</a> and it will detect what kind of system you are using.<br>If you are on an iOS or Android device, you look up slack in the App Store or Google Play Store.<br><br><a href="https://play.google.com/store/apps/details?id=com.Slack&amp;hl=en" target="_blank"><img src="images/playstore.png" width="30%"></a><a href="https://www.google.com/url?q=https%3A%2F%2Fitunes.apple.com%2Fnz%2Fapp%2Fslack-business-communication-for-teams%2Fid618783545%3Fmt%3D8&amp;sa=D&amp;sntz=1&amp;usg=AFQjCNGUk0rvmUs6PbJVLeTWUUCBnFx2Xg" target="_blank"><img src="images/appstore.png" width="30%"></a>');


DROP TABLE IF EXISTS `Menu_Bar`;
CREATE TABLE `Menu_Bar` (
  `Content_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content` varchar(200) NOT NULL,
  `Link` varchar(2000) NOT NULL,
  PRIMARY KEY (`Content_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `Menu_Bar` (`Content_ID`, `Content`, `Link`) VALUES
(1, 'Home', 'index.php');
INSERT INTO `Menu_Bar` (`Content_ID`, `Content`, `Link`) VALUES
(2, 'Info', 'info.php');
INSERT INTO `Menu_Bar` (`Content_ID`, `Content`, `Link`) VALUES
(3, 'Setup Guides', 'setup.php');
INSERT INTO `Menu_Bar` (`Content_ID`, `Content`, `Link`) VALUES
(4, 'Quick Links', 'links.php');
INSERT INTO `Menu_Bar` (`Content_ID`, `Content`, `Link`) VALUES
(5, 'Google Drive', 'https://drive.google.com/drive/u/0/my-drive');
INSERT INTO `Menu_Bar` (`Content_ID`, `Content`, `Link`) VALUES
(6, 'Admin Login', 'login.php');


DROP TABLE IF EXISTS `Dropdown`;

DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `Content_ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(2000) NOT NULL,
  `imgsrc_1` varchar(2000),
  `tag_1` varchar(2000),
  `taglink_1` varchar(2000),
  `imgsrc_2` varchar(2000),
  `tag_2` varchar(2000),
  `taglink_2` varchar(2000),
  `imgsrc_3` varchar(2000),
  `tag_3` varchar(2000),
  `taglink_3` varchar(2000),
  PRIMARY KEY (`Content_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `links` (`Content_ID`, `title`, `imgsrc_1`, `tag_1`, `taglink_1`, `imgsrc_2`, `tag_2`, `taglink_2`, `imgsrc_3`, `tag_3`, `taglink_3`) VALUES
(1, 'Toi Ohomai/University of Waikato Sites', 'images/moodle.gif', 'Toi Ohomai Moodle','http://moodle2.boppoly.ac.nz/','images/toiohomai.jpg','Toi Ohomai Website','https://www.boppoly.ac.nz/','images/waikatouni.jpg','University of Waikato Website','http://www.waikato.ac.nz/');
INSERT INTO `links` (`Content_ID`, `title`, `imgsrc_1`, `tag_1`, `taglink_1`, `imgsrc_2`, `tag_2`, `taglink_2`, `imgsrc_3`, `tag_3`, `taglink_3`) VALUES
(2, 'Student Resources', 'images/lyndalogo.png', 'Lynda.com','https://www.lynda.com/','images/office365.jpg','Office365','https://login.microsoftonline.com/login.srf?wa=wsignin1.0&rpsnv=4&ct=1490127376&rver=6.7.6640.0&wp=MCMBI&wreply=https%3a%2f%2fportal.office.com%2flanding.aspx%3ftarget%3d%252fOLS%252fMySoftware.aspx&lc=1033&id=501392&msafed=0&client-request-id=67ac31cf-d643-4a1e-8c34-85f1e60bed2e','images/imagine.jpg','Microsoft Imagine','https://e5.onthehub.com/WebStore/Security/Signin.aspx?ws=20830094-5e9b-e011-969d-0030487d8897&vsro=8&rurl=%2fWebStore%2fProductsByMajorVersionList.aspx%3fws%3d20830094-5e9b-e011-969d-0030487d8897');
INSERT INTO `links` (`Content_ID`, `title`, `imgsrc_1`, `tag_1`, `taglink_1`, `imgsrc_2`, `tag_2`, `taglink_2`, `imgsrc_3`, `tag_3`, `taglink_3`) VALUES
(3, 'DAC 5 & 6 Resources', 'images/slack.png', 'DAC 5 & 6 Slack','https://to-bcs.slack.com','images/bitbucket.png','Bitbucket','https://bitbucket.org/#','images/timetable.gif','2017 Timetables','https://docs.google.com/spreadsheets/d/1GtztejZAiyF_j_qfgGb7Pc7d3zYOLXhmlvWoZ2jifXk/edit#gid=2070798713');
INSERT INTO `links` (`Content_ID`, `title`, `imgsrc_1`, `tag_1`, `taglink_1`, `imgsrc_2`, `tag_2`, `taglink_2`, `imgsrc_3`, `tag_3`, `taglink_3`) VALUES
(4, 'Jeff\'s Sites', 'images/toiohomai.jpg', 'COMP5002','https://sites.google.com/bcs.net.nz/comp5002/','images/toiohomai.jpg','COMP6002','https://sites.google.com/bcs.net.nz/comp6002/','images/toiohomai.jpg','COMP6008','https://sites.google.com/bcs.net.nz/comp6008/');


DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `tbl_admin` (`ID`, `USER`, `PASSWRD`) VALUES
(1, 'admin', 'admin1');