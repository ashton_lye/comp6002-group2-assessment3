<?php 
    date_default_timezone_set ('Pacific/Auckland');
    
    define("DBHOST", "localhost");
    define("DBUSER", "root");
    define("DBPASS", "root");
    define("DBNAME", "comp6002_17a_assn3");


function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

//////////////////////////////////////
//////         NAV BAR           /////
//////////////////////////////////////

function get_nav_items() {
    
    $db = connection();
    $sql = "SELECT * FROM Menu_Bar";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['Content_ID'],
            "content" => $row['Content'],
            "link" => $row['Link'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

function show_nav_items($data, $type) {
    
    $array = json_decode($data, True);
    
    $idOutput = "";
    $contentOutput = "";
    $linkOutput = "";
    
    if ($type == "list") {
        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
        
                $idOutput = "";
                $contentOutput = "";
                $linkOutput = "";

                $idOutput .=$array[$i]["id"];             
                $contentOutput .=$array[$i]['content'];
                $linkOutput .=$array[$i]['link'];

                ?><option value="<?php echo $contentOutput ?>"><?php echo $contentOutput ?></option><?php
            }
        }
    }
}

function addNavRecord() {

    if(isset($_POST['addNavItem']))
    {
        $db = connection();

        $newContent = $db->real_escape_string($_POST['newContent']);
        $link = $db->real_escape_string($_POST['link']);

        $stmt = $db->prepare("INSERT INTO Menu_Bar (Content, Link) VALUES (?, ?)");
        $stmt->bind_param("ss", $newContent, $link);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

function removeNavRecord() {

    if(isset($_POST['removeNavRecord']))
    {
        $db = connection();

        $content = $db->real_escape_string($_POST['content']);

        

        $stmt = $db->prepare("DELETE FROM Menu_Bar WHERE Content = ?");
        $stmt->bind_param("s", $content);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

//////////////////////////////////////
//////         INFO PAGE         /////
//////////////////////////////////////

function get_info_items($table) {
    
    $db = connection();
    $sql = "SELECT * FROM $table";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['Content_ID'],
            "section" => $row['Section'],
            "heading" => $row['Heading'],
            "content" => $row['Content'],
            "table" => $table
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//////      INFO EDITING        //////

function editInfoRecord($id, $table) {

    if(isset($_POST['updateInfoItem']))
    {
        $db = connection();

        $section = $db->real_escape_string($_POST['section']);
        $heading = $db->real_escape_string($_POST['heading']);
        $content = $db->real_escape_string($_POST['content']);

        $sql = "UPDATE $table SET Section='".$section."', Heading='".$heading."', Content='".$content."' WHERE Content_ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

//////      Add Record       //////

function addInfoRecord($table) {

    if(isset($_POST['addInfoItem']))
    {
        $db = connection();

        $section = $db->real_escape_string($_POST['section']);
        $heading = $db->real_escape_string($_POST['heading']);
        $content = $db->real_escape_string($_POST['content']);

        $stmt = $db->prepare("INSERT INTO $table (Section, Heading, Content) VALUES (?, ?, ?)");
        $stmt->bind_param("sss", $section, $heading, $content);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

//////     Remove Record    //////

function removeRecord($id, $table) {

    if(isset($_POST['removeRecord']))
    {
        $db = connection();

        $stmt = $db->prepare("DELETE FROM $table WHERE Content_ID = ?");
        $stmt->bind_param("s", $id);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

//////////////////////////////////////
//////        INDEX PAGE        //////
//////////////////////////////////////

function get_index_items($table) {
    
    $db = connection();
    $sql = "SELECT * FROM $table";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['Content_ID'],
            "heading" => $row['Heading'],
            "content" => $row['Content'],
            "button" => $row['Button'],
            "link" => $row['Link'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

function editIndexRecord($id) {

    if(isset($_POST['updateIndexItem']))
    {
        $db = connection();

        $heading = $db->real_escape_string($_POST['heading']);
        $content = $db->real_escape_string($_POST['content']);
        $button = $db->real_escape_string($_POST['button']);
        $link = $db->real_escape_string($_POST['link']);

        $sql = "UPDATE Home_Page SET Heading='".$heading."', Content='".$content."', Button='".$button."', Link='".$link."' WHERE Content_ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}


//////////////////////////////////////
//////        LINKS PAGE        //////
//////////////////////////////////////

function get_links_items($table) {
    
    $db = connection();
    $sql = "SELECT * FROM $table";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
        
            "id" => $row['Content_ID'],
            "heading" => $row['title'],
            "table" => $table,
            
            //Image 1 function
            "imgsrc_1" => $row['imgsrc_1'],
            "tag_1" => $row['tag_1'],
            "taglink_1" => $row['taglink_1'],
            
            //Image 2 function
            "imgsrc_2" => $row['imgsrc_2'],
            "tag_2" => $row['tag_2'],
            "taglink_2" => $row['taglink_2'],
            
            //Image 3 function
            "imgsrc_3" => $row['imgsrc_3'],
            "tag_3" => $row['tag_3'],
            "taglink_3" => $row['taglink_3'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

function editLinksRecord($id) {

    if(isset($_POST['updateLinksItem']))
    {
        $db = connection();

        $heading = $db->real_escape_string($_POST['heading']);
        $imgsrc1 = $db->real_escape_string($_POST['imgsrc1']);
        $tag1 = $db->real_escape_string($_POST['tag1']);
        $taglink1 = $db->real_escape_string($_POST['taglink1']);
        $imgsrc2 = $db->real_escape_string($_POST['imgsrc2']);
        $tag2 = $db->real_escape_string($_POST['tag2']);
        $taglink2 = $db->real_escape_string($_POST['taglink2']);
        $imgsrc3 = $db->real_escape_string($_POST['imgsrc3']);
        $tag3 = $db->real_escape_string($_POST['tag3']);
        $taglink3 = $db->real_escape_string($_POST['taglink3']);

        $sql = "UPDATE links SET title='".$heading."', imgsrc_1='".$imgsrc1."', tag_1='".$tag1."', taglink_1='".$taglink1."', imgsrc_2='".$imgsrc2."', tag_2='".$tag2."', taglink_2='".$taglink2."', imgsrc_3='".$imgsrc3."', tag_3='".$tag3."', taglink_3='".$taglink3."' WHERE Content_ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("links.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

function addLinksRecord() {

    if(isset($_POST['addLinksItem']))
    {
        $db = connection();

        $heading = $db->real_escape_string($_POST['heading']);
        $imgsrc1 = $db->real_escape_string($_POST['imgsrc1']);
        $tag1 = $db->real_escape_string($_POST['tag1']);
        $taglink1 = $db->real_escape_string($_POST['taglink1']);
        $imgsrc2 = $db->real_escape_string($_POST['imgsrc2']);
        $tag2 = $db->real_escape_string($_POST['tag2']);
        $taglink2 = $db->real_escape_string($_POST['taglink2']);
        $imgsrc3 = $db->real_escape_string($_POST['imgsrc3']);
        $tag3 = $db->real_escape_string($_POST['tag3']);
        $taglink3 = $db->real_escape_string($_POST['taglink3']);

        $stmt = $db->prepare("INSERT INTO links (title, imgsrc_1, tag_1, taglink_1, imgsrc_2, tag_2, taglink_2, imgsrc_3, tag_3, taglink_3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssss", $heading, $imgsrc1, $tag1, $taglink1, $imgsrc2, $tag2, $taglink2, $imgsrc3, $tag3, $taglink3);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("links.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}    
                            
//////////////////////////////////////
//////     ADMIN FUNCTIONS      //////
//////////////////////////////////////          

//ADMIN LOGIN
    
function loginAdmin() {

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = "";
        $pass = "";

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_admin WHERE USER = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['USER'],
                "pass" => $row['PASSWRD']
            );
        }

       
        if(mysqli_num_rows($result)>0){
        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("index.php");
        }

        }
        else
        {
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
            
        }
         $result->free();
        $db->close();
        
        
    }
}

// Admin Logout

function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}
function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}


?>

