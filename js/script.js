function loadDoc(page) 
{
  //Setup for admin page  
    var phplink = "";
    if (page == 'indexAdmin' || page == 'index')
    {
        phplink = "index2.php"
    }
    else if(page == 'infoAdmin' || page == 'info')
    {
        phplink = "info2.php";
    }
    else if(page == 'setupAdmin' || page == 'setup')
    {
        phplink = "setup2.php";
    }
    else if(page == 'linksAdmin' || page == 'links')
    {
        phplink = "links2.php"
    }

    //console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
        {

        console.log('xhr',xhttp)

        var result = JSON.parse(this.responseText);
        //var result = JSON.parse('[{"id":"1","section":"Pandora Info","heading":"Your Files","content":"The corporate network (the library and classrooms other than the 3rd floor) is completely separate from the Pandora network (DT219, DT303, DT308 and DT312). Both networks have an network drive for you to store you files in and both networks refer to that drive as H-Drive, however they are different and do not relate to each other in anyway. By using your Google Apps account you are able to access your files from any machine using a web browser and you will not be limited by this.","table":"info_page"},{"id":"2","section":"","heading":"Toi Ohomai Wi-Fi Network","content":"The student Wi-Fi is also completely separate from the Pandora Network. If you are using your own device or connect your mobile device, the easiest way to access your files is by using Google Apps. The Wifi network basically allows you to browse the web. There are some exceptions, but you will hear about that in the Web Development papers you will do in your degree.","table":"info_page"},{"id":"3","section":"","heading":"Can I access the anything on the Pandora Network from Home?","content":"Simple answer - No you cannot. However your course material is on Moodle and you can put files on Google drive and you are able to access what you need from there. In terms of software, everything we use in the labs is available for free either through MS Imagine or a student service or the program is actually free to download.","table":"info_page"}]');
        
        

        var a = document.getElementById('content');
        var b = "";

        for(var i = 0; i < result.length; i++)
        {
            //Index when logged in as Admin
            if (page === 'indexAdmin')
            {
                b += "<h1>"+result[i].heading+"</h1>";
                b += "<p>"+result[i].content+"</p>";
                b += "<br><p><a class='btn btn-primary btn-lg infobutton' href="+result[i].link+" role='button'>"+result[i].button+"</a></p>";
                b += "<br><br><a class='btn btn-warning btn-lg infobutton' href= 'editindex.php?id="+result[i].id+"' role='button'> Edit Content </a>";
            } 
            //Index when not logged in as Admin
            else if (page === 'index')
            {
                b += "<h1>"+result[i].heading+"</h1>";
                b += "<p>"+result[i].content+"</p>";
                b += "<br><p><a class='btn btn-primary btn-lg infobutton' href="+result[i].link+" role='button'>"+result[i].button+"</a></p>";
            }
            //Info/Setup Pages when logged in as Admin
            else if (page === 'infoAdmin' || page === 'setupAdmin')
            {
                b += "<h1>"+result[i].section+"</h1>";
                b += "<div class='panel panel-primary'>";
                b += "<div class='panel-heading infopanel'>";
                b += "<h3 class='panel-title'>"+result[i].heading+"</h3></div>";
                b += "<div class='panel-body'>"+result[i].content+"<br><br>";
                b += "<a class='btn btn-warning btn-lg infobutton' href= 'editinfo.php?id="+result[i].id+"&table="+result[i].table+"' role='button'> Edit Panel </a>";
                b += "<a class='btn btn-danger btn-lg infobutton' href= 'deleterecord.php?id="+result[i].id+"&table="+result[i].table+"' role='button'> Delete Panel </a></div></div>";
            }
            //Info/Setup Pages when not logged in as Admin
            else if (page === 'info' || page === 'setup')
            {
                b += "<h1>"+result[i].section+"</h1>";
                b += "<div class='panel panel-primary'>";
                b += "<div class='panel-heading infopanel'>";
                b += "<h3 class='panel-title'>"+result[i].heading+"</h3></div>";
                b += "<div class='panel-body'>"+result[i].content+"</div></div>";
            }
            //Links Page when logged in as Admin
            else if (page === 'linksAdmin')
            {
                b += "<div class='panel panel-default'>";
                b += "<div class='panel-heading'>";
                b += "<h4>"+result[i].heading+"</h4></div>";
                b += "<div class='panel-body'>";
                b += "<div class='row'>";

                b += "<div class='col-md-4'>";
                b += "<div class='thumbnail'>";
                b += "<a href="+result[i].taglink_1+">";
                b += "<img src="+result[i].imgsrc_1+" style='width:100%'>";
                b += "<div class='caption'><p>"+result[i].tag_1+"</p></div>";
                b += "</a><div></div></div></div>";

                b += "<div class='col-md-4'>";
                b += "<div class='thumbnail'>";
                b += "<a href="+result[i].taglink_2+">";
                b += "<img src="+result[i].imgsrc_2+" style='width:100%'>";
                b += "<div class='caption'><p>"+result[i].tag_2+"</p></div>";
                b += "</a><div></div></div></div>";

                b += "<div class='col-md-4'>";
                b += "<div class='thumbnail'>";
                b += "<a href="+result[i].taglink_3+">";
                b += "<img src="+result[i].imgsrc_3+" style='width:100%'>";
                b += "<div class='caption'><p>"+result[i].tag_3+"</p></div>";
                b += "</a><div></div></div></div>";
                
                b += "</div>";
                b += "<a class='btn btn-warning btn-lg infobutton' href= 'editlinks.php?id="+result[i].id+"' role='button'> Edit Panel </a>";
                b += "<a class='btn btn-danger btn-lg infobutton' href= 'deleterecord.php?id="+result[i].id+"&table="+result[i].table+"' role='button'> Delete Panel </a>";
                b += "</div></div></div>";
            } 
            //Links Page when not logged in as Admin
            else if (page === 'links')
            {
                b += "<div class='panel panel-default'>";
                b += "<div class='panel-heading'>";
                b += "<h4>"+result[i].heading+"</h4></div>";
                b += "<div class='panel-body'>";
                b += "<div class='row'>";

                b += "<div class='col-md-4'>";
                b += "<div class='thumbnail'>";
                b += "<a href="+result[i].taglink_1+">";
                b += "<img src="+result[i].imgsrc_1+" style='width:100%'>";
                b += "<div class='caption'><p>"+result[i].tag_1+"</p></div>";
                b += "</a><div></div></div></div>";

                b += "<div class='col-md-4'>";
                b += "<div class='thumbnail'>";
                b += "<a href="+result[i].taglink_2+">";
                b += "<img src="+result[i].imgsrc_2+" style='width:100%'>";
                b += "<div class='caption'><p>"+result[i].tag_2+"</p></div>";
                b += "</a><div></div></div></div>";

                b += "<div class='col-md-4'>";
                b += "<div class='thumbnail'>";
                b += "<a href="+result[i].taglink_3+">";
                b += "<img src="+result[i].imgsrc_3+" style='width:100%'>";
                b += "<div class='caption'><p>"+result[i].tag_3+"</p></div>";
                b += "</a><div></div></div></div>";
                
                b += "</div>";
                b += "</div></div></div>";
            }
        }
        if (page === 'infoAdmin' || page === 'setupAdmin' || page === 'linksAdmin')
        {
            b += "<a class='btn btn-primary btn-lg infobutton' href='addlinks.php' role='button'>Add Panel</a>";
        }

        a.innerHTML = b;
        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

//Nav Bar
function loadNav(page) 
{
    var phplink = "nav2.php";

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
        {
        console.log('xhr',xhttp)

        var result = JSON.parse(this.responseText);
        console.log(result);

        var a = document.getElementById('navDiv');
        var b = "";

        b += "<div class='container'><div class='row'>";
        b += "<nav class='navbar navbar-default'>";
        b += "<div class='container-fluid'>";
        b += "<div class='navbar-header'>";
        b += "<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>";
        b += "<span class='sr-only'>Toggle navigation</span>";
        b += "<span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span></button>";
        b += "<a class='navbar-brand' href='index.php'>BCS Network</a></div>";
        b += "<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>";
        b += "<ul class='nav navbar-nav'>";

        for(var i = 0; i < result.length; i++)
        {
            b += "<li><a href="+result[i].link+">"+result[i].content+"</a></li>";
        }
        if (page == 'navAdmin')
        {
            b += "<li><a href='editnav.php'>Edit Nav Bar</a></li>";
        }

        b += "</ul></div></div></nav></div>";

        a.innerHTML = b;
        }

    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}
