<?php include_once('functions/functions.php');
session_start();
logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Pandora Network - BCS.net.nz</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <?php if( $_SESSION['login'] == TRUE )
    {     
        ?><body onload="loadDoc('linksAdmin'); loadNav('navAdmin')"><?php                      
    }
    else
    {
        ?><body onload="loadDoc('links'); loadNav('nav')"><?php
    }?>
    <!--Header Image-->
        <div class="header-image">
            <img src="images/logo1.png" class="img-responsive">
        </div>
        
        <div id="navDiv">
        </div>
        
        <div class='container'>
            <div id='content'>
            </div>
        </div>

        <!--footer-->
        <footer class="footer">
            <div class="container">
                <?php 
                    if( $_SESSION['login'] == TRUE )
                    {     
                        ?>
                        <br>
                        <form method='POST' >
                            <input class="btn btn-default" type="submit" name="logout" value="logout">
                        </form>
                        <span class="text-muted">Demo BCS.net Design - Group 2 - Ashton Lye, Cody Ludwig, Liam Mason-Webb</span><?php
                    }
                    else
                    {
                        ?><span class="text-muted">Demo BCS.net Design - Group 2 - Ashton Lye, Cody Ludwig, Liam Mason-Webb</span><?php
                    }
                ?>
            </div>
        </footer>
        
    <script src="js/script.js"></script>
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>