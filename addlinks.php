<?php include_once('functions/functions.php');

session_start();
addLinksRecord();
logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Pandora Network - BCS.net.nz</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <?php if( $_SESSION['login'] == TRUE )
    {     
        ?><body onload="loadNav('navAdmin')"><?php                      
    }
    else
    {
        ?><body onload="loadNav('nav')"><?php
    }?>
    <body>
        <!--Header Image-->
        <div class="header-image">
            <img src="images/logo1.png" class="img-responsive">
        </div>

         <div id="navDiv">
        </div>       
        <div class="container">
        
        <?php 
            if( $_SESSION['login'] == TRUE )
            {     
                ?>
                <div class="jumbotron">
                    <h1>Add a Links Panel</h1>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form method="POST">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Change  Heading</span>
                                    <input type="text" class="form-control" name="heading" placeholder="Section Heading" aria-describedby="basic-addon1">
                                </div>    
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <img src="images/placeholder.png" style="width:100%">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Source:</span>
                                            <input type="text" class="form-control" name="imgsrc1" placeholder="Add A Link to the Image" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="caption">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Tag:</span>
                                            <input type="text" class="form-control" name="tag1" placeholder="Image Tag" aria-describedby="basic-addon1">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Link:</span>
                                            <input type="text" class="form-control" name="taglink1" placeholder="Link" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <img src="images/placeholder.png" style="width:100%">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Source:</span>
                                            <input type="text" class="form-control" name="imgsrc2" placeholder="Add A Link to the Image" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="caption">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Tag:</span>
                                            <input type="text" class="form-control" name="tag2" placeholder="Image Tag" aria-describedby="basic-addon1">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Link:</span>
                                            <input type="text" class="form-control" name="taglink2" placeholder="Link" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <img src="images/placeholder.png" style="width:100%">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Source:</span>
                                            <input type="text" class="form-control" name="imgsrc3" placeholder="Add A Link to the Image" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="caption">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Tag:</span>
                                            <input type="text" class="form-control" name="tag3" placeholder="Image Tag" aria-describedby="basic-addon1">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Image Link:</span>
                                            <input type="text" class="form-control" name="taglink3" placeholder="Link" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <button type="submit" name="addLinksItem" class="btn btn-success">Add Panel</button>
                        </form>
                    </div><?php
            }
            else
            {
                ?><div class="panel panel-primary">
                    <div class="panel-heading infopanel">
                        <h3 class="panel-title">Error: You do not have access to this page</h3>
                    </div>
                    <div class="panel-body">
                    <div class="panel-body">
                        <p>Please try again after logging in.</p>
                    </div>
                  </div><?php
            }
        ?>
    
        <!--footer-->
        <footer class="footer">
            <div class="container">
                <?php 
                    if( $_SESSION['login'] == TRUE )
                    {     
                        ?><form method='POST' >
                            <input class="btn btn-default" type="submit" name="logout" value="logout">
                        </form>
                        <span class="text-muted">Demo BCS.net Design - Group 2 - Ashton Lye, Cody Ludwig, Liam Mason-Webb</span><?php
                    }
                    else
                    {
                        ?><span class="text-muted">Demo BCS.net Design - Group 2 - Ashton Lye, Cody Ludwig, Liam Mason-Webb</span><?php
                    }
                ?>
            </div>
        </footer>
    
    <script src= "js/script.js"></script>
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>